<?php
	
	namespace Sixnapps\PortoTemplateBundle\DependencyInjection;
	
	use Symfony\Component\Config\Definition\Builder\TreeBuilder;
	use Symfony\Component\Config\Definition\ConfigurationInterface;
	
	class Configuration implements ConfigurationInterface
	{
		/**
		 * @return TreeBuilder
		 */
		public function getConfigTreeBuilder()
		{
			$treeBuilder = new TreeBuilder( 'sixnapps_porto_template' );
			
			if ( method_exists( $treeBuilder, 'getRootNode' ) ) {
				$rootNode = $treeBuilder->getRootNode();
			} else {
				// BC layer for symfony/config 4.1 and older
				$rootNode = $treeBuilder->root( "sixnapps_porto_template" );
			}
			
			$rootNode
				->children()
					->arrayNode( 'template' )
						->children()
							->arrayNode( 'footer' )
								->children()
									->scalarNode( 'type' )->end()
								->end()
							->end()
						->end()
					->end()
				->end()
			;
			
			return $treeBuilder;
		}
	}
