<?php
	
	namespace Sixnapps\PortoTemplateBundle\Controllers;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	/**
	 * Class callToActionController
	 *
	 * @package Sixnapps\PortoTemplateBundle\Controllers
	 */
	class ElementsCallToActionController extends AbstractController
	{
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function show()
		{
			return $this->render('@SixnappsPortoTemplate/elements-call-to-action.html.twig');
		}
	}
