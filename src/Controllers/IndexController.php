<?php
	
	namespace Sixnapps\PortoTemplateBundle\Controllers;
	
	use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
	
	/**
	 * Class IndexController
	 *
	 * @package Sixnapps\PortoTemplateBundle\Controllers
	 */
	class IndexController extends AbstractController
	{
		/**
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
		public function index()
		{
			return $this->render( '@SixnappsPortoTemplate/index.html.twig' );
		}
		
		
	}
