<?php
	namespace Sixnapps\PortoTemplateBundle\Templating;
	
	use Symfony\Component\Templating\EngineInterface;
	
	class Templating
	{
		private $templating;
		
		private $components;
		
		
		public function __construct( EngineInterface $templating )
		{
			$this->templating = $templating;
			$this->components = [
				'title'         => 'title/title',
				'header'        => 'header/header',
				'sticky-footer' => 'footer/sticky-footer',
			];
		}
		
		
		/**
		 * @param $item
		 * @param $data
		 *
		 * @return mixed
		 */
		public function render( $item, $data )
		{
			if ( isset( $this->components[ $item ] ) && $this->templating->exists( '@SixnappsPortoTemplate/components/' . $this->components[ $item ] . '.html.twig' ) ) {
				return $this->templating->render( '@SixnappsPortoTemplate/components/' . $this->components[ $item ] . '.html.twig', $data );
			}
			return $this->templating->render( '@SixnappsPortoTemplate/components/not-found.html.twig', [
				'name' => $item,
			] );
		}
	}
