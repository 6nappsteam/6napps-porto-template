<?php
	
	namespace Sixnapps\PortoTemplateBundle\Twig;
	
	use Sixnapps\PortoTemplateBundle\Templating\Templating;
	use Symfony\Component\HttpKernel\KernelInterface;
	
	/**
	 * Class TwigExtension
	 *
	 * @package Sixnapps\PortoTemplateBundle\Twig
	 */
	class TwigExtension extends \Twig_Extension
	{
		/**
		 * @var KernelInterface
		 */
		private $kernel;
		
		/**
		 * @var Templating
		 */
		private $templating;
		
		
		/**
		 * TwigExtension constructor.
		 *
		 * @param KernelInterface $kernel
		 * @param Templating      $templating
		 */
		public function __construct( KernelInterface $kernel, Templating $templating )
		{
			$this->kernel     = $kernel;
			$this->templating = $templating;
		}
		
		
		/**
		 * @return array|\Twig_Function[]
		 */
		public function getFunctions()
		{
			return [
				new \Twig_SimpleFunction( 'bundleExists', [ $this, 'bundleExists' ], [ 'is_safe' => [ 'html' ] ] ),
				new \Twig_SimpleFunction( 'porto_component', [ $this, 'porto_component' ], [ 'is_safe' => [ 'html' ] ] ),
			];
		}
		
		
		/**
		 * @param       $item
		 * @param array $data
		 *
		 * @return mixed
		 */
		public function porto_component( $item, $data = [] )
		{
			return $this->templating->render( $item, $data );
		}
		
		
		/**
		 * @param $bundle
		 *
		 * @return bool
		 */
		public function bundleExists( $bundle )
		{
			return array_key_exists(
				$bundle,
				$this->kernel->getBundles()
			);
		}
		
	}
